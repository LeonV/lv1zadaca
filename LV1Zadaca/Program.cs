﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1Zadaca
{
    class Program
    {
        static void Main(string[] args)
        {
            Note defaultNote = new Note();
            Note myNote = new Note("Zadaca iz RPPOON", 5);
            Note otherNote = new Note("Okupljanje na kavi", "Petar", 2);

            Console.WriteLine(defaultNote.GetAuthor() + ", " + defaultNote.GetText());
            Console.WriteLine(myNote.GetAuthor() + ", " + myNote.GetText());
            Console.WriteLine(otherNote.GetAuthor() + ", " + otherNote.GetText());

            ToDo duties = new ToDo();
            Console.WriteLine("Enter how many notes you wish to enter: ");
            int numberOfNotes = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < numberOfNotes; i++)
            {
                Console.WriteLine("Name of author: ");
                string tempAuthor = (Console.ReadLine());
                Console.WriteLine("Note text: ");
                string tempText = (Console.ReadLine());
                Console.WriteLine("Importance of note: ");
                int tempPriority = Convert.ToInt32(Console.ReadLine());
                duties.AddNote(new Note(tempText, tempAuthor, tempPriority));
            }
            Console.WriteLine(duties.ToString());
            int maxPriority = 0;
            for (int i = 0; i < duties.GetNotesLenght(); i++)
            {
                if (maxPriority < duties.GetNoteAt(i).Priority)
                {
                    maxPriority = duties.GetNoteAt(i).Priority;
                }
            }
            for (int i = 0; i < duties.GetNotesLenght(); i++)
            {
                if (maxPriority == duties.GetNoteAt(i).Priority)
                {
                    duties.RemoveNoteAt(i);
                }
            }
            Console.WriteLine(duties.ToString());
        }
    }
}