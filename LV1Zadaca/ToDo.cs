﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1Zadaca
{
    class ToDo
    {
        private List<Note> notes = new List<Note>();
        public Note GetNoteAt(int index) { return notes[index]; }
        public int GetNotesLenght() { return notes.Count; }
        public void AddNote(Note note) { notes.Add(note); }
        public void RemoveNoteAt(int index) { notes.RemoveAt(index); }

        public override string ToString()
        {
            String temp = "";
            foreach (Note note in notes)
            {
                temp = temp + System.Environment.NewLine + note.ToString();
            }
            return temp;
        }
    }
}
