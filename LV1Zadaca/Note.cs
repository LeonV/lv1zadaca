﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1Zadaca
{
    class Note
    {
        private String text;
        private String author;
        private int priority;
        public String GetText() { return text; }
        public void SetText(String Text) { this.text = Text; }
        public String GetAuthor() { return author; }
        private void SetAuthor(String Author) { this.author = Author; }
        public int GetPriority() { return priority; }
        public void SetPriority(int Priority) { this.priority = Priority; }
        public String Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public String Author
        {
            get { return this.author; }
            private set { this.author = value; }
        }
        public int Priority
        {
            get { return this.priority; }
            set { this.priority = value; }
        }
        public Note() { this.text = "Note Text"; this.author = "Me"; this.priority = 0; }
        public Note(String Text, int Priority) { this.text = Text; this.author = "Me"; this.priority = Priority; }
        public Note(String Text, String Author, int Priority) { this.text = Text; this.author = Author; this.priority = Priority; }
        public override string ToString()
        {
            return this.author + ", " + this.text;
        }
    }
}
