﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV1Zadaca
{
    class TimeNote : Note
    {
        private DateTime time;
        public TimeNote(String Text, String Author, int Priority) : base(Text, Author, Priority) { this.time = DateTime.Now; }
        public TimeNote(String Text, String Author, int Priority, DateTime Time) : base(Text, Author, Priority) { this.time = Time; }
        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return base.ToString() + ", " + time.ToString();
        }
    }
}
